document.addEventListener('DOMContentLoaded', () => {
  const testArea = document.getElementById('testArea');
  const scoreDisplay = document.getElementById('scoreDisplay');
  const startButton = document.getElementById('startButton');
  let startTime, endTime;

  startButton.addEventListener('click', startTest);

  function startTest() {
    resetTest();
    testArea.style.backgroundColor = 'red';
    startButton.disabled = true;
    const randomTime = Math.floor(Math.random() * 2000) + 1000; // 1-3 seconds

    setTimeout(() => {
      testArea.style.backgroundColor = 'green';
      startTime = new Date().getTime();
      testArea.addEventListener('click', recordReaction);
    }, randomTime);
  }

  function recordReaction() {
    endTime = new Date().getTime();
    const reactionTime = endTime - startTime;
    scoreDisplay.textContent = `Your reaction time is ${reactionTime} ms`;
    testArea.removeEventListener('click', recordReaction);
    startButton.disabled = false;
  }

  function resetTest() {
    scoreDisplay.textContent = '';
    testArea.style.backgroundColor = 'red';
  }

  testArea.addEventListener('click', () => {
    if (testArea.style.backgroundColor !== 'green') {
      scoreDisplay.textContent = 'Too soon! Wait for green.';
    }
  });
});
